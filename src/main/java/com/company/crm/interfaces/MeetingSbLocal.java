package com.company.crm.interfaces;

import java.util.List;

import com.company.crm.model.Meeting;

public interface MeetingSbLocal {

	public List<Meeting> getCustomerMeetings(Integer customerId);

	public Meeting getMeeting(Integer meetingId, boolean fetchCustomers, boolean fetchConclusions);

}
