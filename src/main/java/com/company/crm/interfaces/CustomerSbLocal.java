package com.company.crm.interfaces;

import java.util.List;

import javax.ejb.Local;

import com.company.crm.model.Customer;

public interface CustomerSbLocal {
	
	public List<Customer> getCustomers(String filterString);
	
	public Customer getCustomer(int id, boolean fetchMeetings);
	
}
