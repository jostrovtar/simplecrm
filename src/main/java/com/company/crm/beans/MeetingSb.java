package com.company.crm.beans;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.company.crm.interfaces.MeetingSbLocal;
import com.company.crm.model.Meeting;

@Stateless
@Local(MeetingSbLocal.class)
public class MeetingSb implements MeetingSbLocal {

	@PersistenceContext(unitName = "crmpu")
	private EntityManager em;

	@Override
	public List<Meeting> getCustomerMeetings(Integer customerId) {

		List<Meeting> list = em.createNamedQuery("Meeting.findByCustomer", Meeting.class)
				.setParameter("cId", customerId).getResultList();

		return list;
	}

	@Override
	public Meeting getMeeting(Integer meetingId, boolean fetchCustomers, boolean fetchConclusions) {

		Meeting m = em.find(Meeting.class, meetingId);

		if (m != null) {

			if (m.getCustomers() != null && fetchCustomers) {
				m.getCustomers().size();
			}

			if (m.getConclusions() != null && fetchConclusions) {
				m.getConclusions().size();
			}

		}

		return m;
	}

}
