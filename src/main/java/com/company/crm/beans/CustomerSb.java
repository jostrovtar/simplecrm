package com.company.crm.beans;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.company.crm.interfaces.CustomerSbLocal;
import com.company.crm.model.Customer;

@Stateless
@Local(CustomerSbLocal.class)
public class CustomerSb implements CustomerSbLocal {

	@PersistenceContext(unitName = "crmpu")
	private EntityManager em;

	@Override
	public List<Customer> getCustomers(String filterString) {

		if ("".equals(filterString)) {
			filterString = null;
		} else if (filterString != null) {
			filterString = "%" + filterString + "%";
		}

		List<Customer> list = em.createNamedQuery("Customer.findFiltered", Customer.class)
				.setParameter("filter", filterString).getResultList();

		return list;
	}

	@Override
	public Customer getCustomer(int id, boolean fetchMeetings) {

		Customer c = em.find(Customer.class, id);

		if (c != null && c.getMeetings() != null && fetchMeetings) {
			c.getMeetings().size();
		}

		return c;
	}

}
