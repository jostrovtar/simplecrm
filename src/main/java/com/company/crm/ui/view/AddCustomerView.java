package com.company.crm.ui.view;

import javax.inject.Inject;

import com.company.crm.ui.common.UserInfo;
import com.company.crm.ui.layout.ContentLayout;
import com.company.crm.ui.layout.RootLayout;
import com.company.crm.ui.navigation.NavigationTree;
import com.vaadin.cdi.CDIView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@CDIView("addCustomer")
public class AddCustomerView extends CustomComponent implements View {

	private static final long serialVersionUID = 1L;

	@Inject
	private UserInfo user;

	@Override
	public void enter(ViewChangeEvent event) {

		// SETUP
		RootLayout rootLayout = new RootLayout();
		ContentLayout contentLayout = new ContentLayout(NavigationTree.addCustomerId, user, getUI().getNavigator());
		rootLayout.addComponent(contentLayout);
		setCompositionRoot(rootLayout);

		// CONTENT
		VerticalLayout subLayout = new VerticalLayout();
		subLayout.setMargin(true);
		subLayout.setSpacing(true);

		subLayout.addComponent(new Label("Add customer view"));

		// ADD CREATED LAYOUT
		contentLayout.addComponent(subLayout);

	}

}
