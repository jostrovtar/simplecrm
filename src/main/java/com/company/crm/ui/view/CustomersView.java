package com.company.crm.ui.view;

import java.util.List;

import javax.inject.Inject;

import com.company.crm.interfaces.CustomerSbLocal;
import com.company.crm.interfaces.MeetingSbLocal;
import com.company.crm.model.Customer;
import com.company.crm.ui.common.UserInfo;
import com.company.crm.ui.layout.ContentLayout;
import com.company.crm.ui.layout.RootLayout;
import com.company.crm.ui.navigation.NavigationTree;
import com.company.crm.ui.window.CustomerMeetingsWindow;
import com.vaadin.cdi.CDIView;
import com.vaadin.data.Item;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@CDIView("customers")
public class CustomersView extends CustomComponent implements View {

	private static final long serialVersionUID = 1L;

	@Inject
	private CustomerSbLocal customerSb;

	@Inject
	private MeetingSbLocal meetingSb;

	@Inject
	private UserInfo user;

	private Table tableCustomers;

	private TextField filterTextField;

	@Override
	public void enter(ViewChangeEvent event) {

		// SETUP
		RootLayout rootLayout = new RootLayout();
		ContentLayout contentLayout = new ContentLayout(NavigationTree.customersId, user, getUI().getNavigator());
		rootLayout.addComponent(contentLayout);
		setCompositionRoot(rootLayout);

		// CONTENT
		VerticalLayout layoutCustomerData = new VerticalLayout();
		layoutCustomerData.setMargin(true);
		layoutCustomerData.setSpacing(true);

		HorizontalLayout layoutFilter = new HorizontalLayout();
		layoutFilter.setSpacing(true);

		Label labelCustFilter = new Label("Filter customers: ");
		filterTextField = new TextField();
		Button filterButton = createButtonFilterCustomers();
		layoutFilter.addComponent(labelCustFilter);
		layoutFilter.addComponent(filterTextField);
		layoutFilter.addComponent(filterButton);

		List<Customer> list = customerSb.getCustomers("");

		tableCustomers = createCustomersTable(list);

		layoutCustomerData.addComponent(layoutFilter);
		layoutCustomerData.addComponent(tableCustomers);

		// ADD CREATED LAYOUT
		contentLayout.addComponent(layoutCustomerData);

	}

	private Table createCustomersTable(List<Customer> list) {

		Table table = new Table();
		table.setHeight("500");
		table.addContainerProperty("id", Integer.class, null);

		table.addContainerProperty("fName", String.class, null);
		table.setColumnHeader("fName", "First name");

		table.addContainerProperty("lName", String.class, null);
		table.setColumnHeader("lName", "Last name");

		table.addContainerProperty("email", String.class, null);
		table.setColumnHeader("email", "Email");

		table.addContainerProperty("phone", String.class, null);
		table.setColumnHeader("phone", "Phone");

		table.addContainerProperty("meetings", Button.class, null);
		table.setColumnHeader("meetings", "Meetings");

		table.setVisibleColumns(new Object[] { "fName", "lName", "email", "phone", "meetings" });

		for (Customer c : list) {
			Object newItemId = table.addItem();
			Item row = table.getItem(newItemId);
			row.getItemProperty("id").setValue(c.getId());
			row.getItemProperty("fName").setValue(c.getFirstName());
			row.getItemProperty("lName").setValue(c.getLastName());
			row.getItemProperty("email").setValue(c.getEmail());
			row.getItemProperty("phone").setValue(c.getPhone());
			row.getItemProperty("meetings").setValue(createShowMeetingsButton(c));

		}

		return table;

	}

	private Button createButtonFilterCustomers() {

		Button b = new Button("Filter");
		b.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {

				tableCustomers.removeAllItems();

				List<Customer> list = customerSb.getCustomers(filterTextField.getValue());

				for (Customer c : list) {
					Object newItemId = tableCustomers.addItem();
					Item row = tableCustomers.getItem(newItemId);
					row.getItemProperty("id").setValue(c.getId());
					row.getItemProperty("fName").setValue(c.getFirstName());
					row.getItemProperty("lName").setValue(c.getLastName());
					row.getItemProperty("email").setValue(c.getEmail());
					row.getItemProperty("phone").setValue(c.getPhone());
					row.getItemProperty("meetings").setValue(createShowMeetingsButton(c));

				}
			}
		});

		return b;
	}

	private Button createShowMeetingsButton(Customer c) {
		Button showCustMeetings = new Button("Show meetings");
		showCustMeetings.setData(c.getId());
		showCustMeetings.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {

				Integer itemId = (Integer) event.getButton().getData();
				Customer c = customerSb.getCustomer(itemId.intValue(), true);

				UI.getCurrent().addWindow(new CustomerMeetingsWindow(c, meetingSb));

			}
		});

		return showCustMeetings;

	}

}
