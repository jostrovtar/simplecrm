package com.company.crm.ui.view;

import javax.inject.Inject;

import com.company.crm.ui.common.UserInfo;
import com.vaadin.annotations.Theme;
import com.vaadin.cdi.CDIUI;
import com.vaadin.cdi.CDIViewProvider;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;

/**
 * This UI is the application entry point. A UI may either represent a browser
 * window (or tab) or some part of a html page where a Vaadin application is
 * embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is
 * intended to be overridden to add component to the user interface and
 * initialize non-component functionality.
 */
@CDIUI("")
@Theme("valo")
public class MyUI extends UI {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private UserInfo user;

	@Inject
	private CDIViewProvider viewProvider;

	@Override
	protected void init(VaadinRequest vaadinRequest) {

		Page.getCurrent().setTitle("CRM");

		Navigator navigator = new Navigator(this, this);
		navigator.addProvider(viewProvider);
		navigator.navigateTo("login");

	}

}
