package com.company.crm.ui.view;

import javax.inject.Inject;

import com.company.crm.ui.common.UserInfo;
import com.vaadin.cdi.CDIView;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@CDIView("login")
public class LoginView extends CustomComponent implements View, ClickListener {

	@Inject
	private UserInfo user;

	private TextField usernameField;
	private PasswordField passwordField;
	private Button loginButton;

	private Navigator navigator;

	@Override
	public void enter(ViewChangeEvent event) {

		navigator = getUI().getNavigator();

		if (user != null && user.getId() != -1) {
			navigator.navigateTo("customers");
		}

		usernameField = new TextField("Username");
		passwordField = new PasswordField("Password");
		loginButton = new Button("Login");
		loginButton.addClickListener(this);
		loginButton.setClickShortcut(KeyCode.ENTER);

		VerticalLayout layout = new VerticalLayout();
		setCompositionRoot(layout);
		layout.setSizeFull();
		layout.setMargin(true);
		layout.setSpacing(true);

		layout.addComponent(usernameField);
		layout.addComponent(passwordField);
		layout.addComponent(loginButton);

	}

	@Override
	public void buttonClick(ClickEvent event) {

		String username = usernameField.getValue();
		String password = passwordField.getValue();

		if ("admin".equals(username) || "admin".equals(password)) {

			user.setUserName(username);
			user.setFirstName("Admin");
			user.setLastName("");
			user.setId(1);

			if (navigator != null) {
				navigator.navigateTo("customers");
			}

		} else {
			Notification.show("Incorrect user data", "", Notification.Type.ERROR_MESSAGE);
		}

	}

}
