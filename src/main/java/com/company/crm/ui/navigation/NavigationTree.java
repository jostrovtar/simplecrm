package com.company.crm.ui.navigation;

import com.vaadin.data.Property;
import com.vaadin.ui.Tree;
import com.vaadin.ui.UI;

public class NavigationTree extends Tree {

	private static final long serialVersionUID = 1L;

	public static String customersId = "customers";

	public static String addCustomerId = "addCustomer";

	public static String meetingsId = "meetings";

	public static String addMeetingId = "addMeetings";

	public NavigationTree(String menuTreeSelItemID) {
		super();

		addItem(customersId);
		setChildrenAllowed(customersId, false);
		setItemCaption(customersId, "Customers");

		addItem(addCustomerId);
		setChildrenAllowed(addCustomerId, false);
		setItemCaption(addCustomerId, "Add customer");

		addItem(meetingsId);
		setChildrenAllowed(meetingsId, false);
		setItemCaption(meetingsId, "Meetings");

		addItem(addMeetingId);
		setChildrenAllowed(addMeetingId, false);
		setItemCaption(addMeetingId, "Add meeting");

		select(menuTreeSelItemID);

		addValueChangeListener(new Property.ValueChangeListener() {

			@Override
			public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {

				if (event == null || event.getProperty() == null || event.getProperty().getValue() == null) {
					return;
				}

				String selectedLeaf = (String) event.getProperty().getValue();

				if (customersId.equals(selectedLeaf)) {
					UI.getCurrent().getNavigator().navigateTo("customers");
				} else if (addCustomerId.equals(selectedLeaf)) {
					UI.getCurrent().getNavigator().navigateTo("addCustomer");
				} else if (meetingsId.equals(selectedLeaf)) {
					UI.getCurrent().getNavigator().navigateTo("meetings");
				} else if (addMeetingId.equals(selectedLeaf)) {
					UI.getCurrent().getNavigator().navigateTo("addMeeting");
				}

			}

		});

	}

}
