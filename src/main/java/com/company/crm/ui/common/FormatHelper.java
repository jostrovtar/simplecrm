package com.company.crm.ui.common;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatHelper {

	public static String formatDateTime(Date date){
		
		if(date == null){
			return "";
		}
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		return dateFormat.format(date);
	}
	
}
