package com.company.crm.ui.panel;

import com.company.crm.model.Meeting;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class MeetingDetailsPanel extends Panel {

	private static final long serialVersionUID = 1L;

	public MeetingDetailsPanel() {
		super();
	}

	public MeetingDetailsPanel(Meeting m) {
		super();

		HorizontalLayout mainLayout = new HorizontalLayout();

		VerticalLayout leftSubLayout = new VerticalLayout();
		leftSubLayout.addComponent(new Label("Title: " + m.getTitle()));
		leftSubLayout.addComponent(new Label("Location: " + m.getLocation()));
		leftSubLayout.addComponent(new Label("Start: " + m.getStart()));
		leftSubLayout.addComponent(new Label("End: " + m.getEnd()));

		VerticalLayout rightSubLayout = new VerticalLayout();
		rightSubLayout.addComponent(new Label("Customers: " + m.getCustomers().size()));
		rightSubLayout.addComponent(new Label("Conclusions: " + m.getConclusions().size()));

		mainLayout.addComponent(leftSubLayout);
		mainLayout.addComponent(rightSubLayout);
		setContent(mainLayout);

	}

}
