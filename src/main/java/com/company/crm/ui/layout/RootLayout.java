package com.company.crm.ui.layout;

import com.vaadin.ui.VerticalLayout;

public class RootLayout extends VerticalLayout {

	public RootLayout() {
		super();
		addComponent(new TitileContentLayout());
		setMargin(true);
	}

}
