package com.company.crm.ui.layout;

import com.company.crm.ui.common.UserInfo;
import com.company.crm.ui.navigation.NavigationTree;
import com.vaadin.navigator.Navigator;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class ContentLayout extends HorizontalLayout {

	private static final long serialVersionUID = 1L;

	private UserInfo u;

	public ContentLayout(String menuTreeSelItemID, UserInfo user, Navigator navigator) {
		setSpacing(true);

		if (user == null || user.getId() == -1) {
			navigator.navigateTo("login");
		}

		u = user;

		Button button = new Button("Logout");
		button.setWidth("160");
		button.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				u.setId(-1);
				navigator.navigateTo("login");
			}
		});

		VerticalLayout menuLayout = new VerticalLayout();
		menuLayout.addComponent(
				new Label("<b>Logged: " + user.getFirstName() + " " + user.getLastName() + "</b>", ContentMode.HTML));
		menuLayout.addComponent(button);
		menuLayout.addComponent(new Label("<b>MENU</b>", ContentMode.HTML));
		menuLayout.addComponent(new NavigationTree(menuTreeSelItemID));

		addComponent(menuLayout);
	}

}
