package com.company.crm.ui.layout;

import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;

public class TitileContentLayout extends HorizontalLayout {

	private static final long serialVersionUID = 1L;

	public TitileContentLayout() {

		super();

		Label labelTitle = new Label("CRM");
		labelTitle.setStyleName("h1");
		addComponent(labelTitle);
	}

}
