package com.company.crm.ui.window;

import com.company.crm.interfaces.MeetingSbLocal;
import com.company.crm.model.Conclusion;
import com.company.crm.model.Customer;
import com.company.crm.model.Meeting;
import com.company.crm.ui.common.FormatHelper;
import com.vaadin.data.Item;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class CustomerMeetingsWindow extends Window {

	private static final long serialVersionUID = 1L;

	private MeetingSbLocal meetingSb;

	private Table tableMeetings;

	private Panel meetingDetailsPanel;

	private final String tableMeetingsHeight = "582";

	private final String tableMeetingsHeightReduced = "300";

	private final String meetingDetailsPanelHeight = "270";

	public CustomerMeetingsWindow(Customer customer, MeetingSbLocal meetingSb) {

		super(customer.getFirstName() + " " + customer.getLastName() + "'s meetings:");
		this.meetingSb = meetingSb;

		center();
		setClosable(false);
		setResizable(false);
		setModal(true);
		setHeight("700");
		setWidth("1000");

		meetingDetailsPanel = new Panel();
		meetingDetailsPanel.setVisible(false);

		tableMeetings = createTableMeetings(customer);

		Button closeButton = createCloseCustomerMeetingsButton();

		Button hidePanelButton = createHideMeetingDetailsButton();

		HorizontalLayout bottomButonsLayout = new HorizontalLayout();
		bottomButonsLayout.setSpacing(true);
		bottomButonsLayout.addComponent(closeButton);
		bottomButonsLayout.addComponent(hidePanelButton);

		VerticalLayout windowMainContent = new VerticalLayout();
		windowMainContent.setSpacing(true);
		windowMainContent.setMargin(true);
		windowMainContent.addComponent(tableMeetings);
		windowMainContent.addComponent(meetingDetailsPanel);
		windowMainContent.addComponent(bottomButonsLayout);

		setContent(windowMainContent);

	}

	private Button createHideMeetingDetailsButton() {

		Button button = new Button("Hide meeting details");
		button.addClickListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				meetingDetailsPanel.setVisible(false);
				tableMeetings.setHeight(tableMeetingsHeight);

			}
		});

		return button;
	}

	private Button createCloseCustomerMeetingsButton() {

		Button button = new Button("Close");
		button.addClickListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				close();
			}
		});

		return button;
	}

	private Table createTableMeetings(Customer customer) {

		Table table = new Table();
		table.setHeight(tableMeetingsHeight);
		table.setWidth("100%");

		table.addContainerProperty("id", Integer.class, null);

		table.addContainerProperty("title", String.class, null);
		table.setColumnHeader("title", "Title");

		table.addContainerProperty("location", String.class, null);
		table.setColumnHeader("location", "Location");

		table.addContainerProperty("start", String.class, null);
		table.setColumnHeader("start", "Start");

		table.addContainerProperty("end", String.class, null);
		table.setColumnHeader("end", "End");

		table.addContainerProperty("details", Button.class, null);
		table.setColumnHeader("details", "Details");

		table.setVisibleColumns(new Object[] { "title", "location", "start", "end", "details" });

		for (Meeting m : customer.getMeetings()) {
			Object newItemId = table.addItem();
			Item row = table.getItem(newItemId);
			row.getItemProperty("id").setValue(m.getId());
			row.getItemProperty("title").setValue(m.getTitle());
			row.getItemProperty("location").setValue(m.getLocation());
			row.getItemProperty("start").setValue(FormatHelper.formatDateTime(m.getStart()));
			row.getItemProperty("end").setValue(FormatHelper.formatDateTime(m.getEnd()));
			row.getItemProperty("details").setValue(createButtonShowMeetingDetails(m));

		}

		table.setSortContainerPropertyId("start");
		table.setSortAscending(false);
		table.sort();

		return table;
	}

	private Button createButtonShowMeetingDetails(Meeting m) {

		Button showDetailsButton = new Button("Show details");
		showDetailsButton.setData(m.getId());
		showDetailsButton.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {

				// Integer itemId = (Integer) event.getButton().getData();
				Meeting meeting = meetingSb.getMeeting(m.getId(), true, true);

				HorizontalLayout mainLayout = new HorizontalLayout();
				mainLayout.setMargin(true);
				mainLayout.setSpacing(true);

				VerticalLayout leftSubLayout = new VerticalLayout();

				leftSubLayout.addComponent(new Label("Title: "));
				Label lTitle = new Label("<b>" + meeting.getTitle() + "</b>", ContentMode.HTML);
				leftSubLayout.addComponent(lTitle);

				leftSubLayout.addComponent(new Label("Location: "));
				Label lLocation = new Label("<b>" + meeting.getLocation() + "</b>", ContentMode.HTML);
				leftSubLayout.addComponent(lLocation);

				leftSubLayout.addComponent(new Label("Start: "));
				Label lStart = new Label("<b>" + FormatHelper.formatDateTime(meeting.getStart()) + "</b>",
						ContentMode.HTML);
				leftSubLayout.addComponent(lStart);

				leftSubLayout.addComponent(new Label("End: "));
				Label lEnd = new Label("<b>" + FormatHelper.formatDateTime(meeting.getEnd()) + "</b>",
						ContentMode.HTML);
				leftSubLayout.addComponent(lEnd);

				VerticalLayout centerSubLayout = new VerticalLayout();
				Table tableCustomers = new Table("All participant customers");
				tableCustomers.setHeight("200");

				tableCustomers.addContainerProperty("fName", String.class, null);
				tableCustomers.setColumnHeader("fName", "First name");

				tableCustomers.addContainerProperty("lName", String.class, null);
				tableCustomers.setColumnHeader("lName", "Last name");

				for (Customer c : meeting.getCustomers()) {
					Object newItemId = tableCustomers.addItem();
					Item row = tableCustomers.getItem(newItemId);
					row.getItemProperty("fName").setValue(c.getFirstName());
					row.getItemProperty("lName").setValue(c.getLastName());
				}
				centerSubLayout.addComponent(tableCustomers);

				VerticalLayout rightSubLayout = new VerticalLayout();
				Table tableConclusions = new Table("All meeting conclusions");
				tableConclusions.setHeight("200");

				tableConclusions.addContainerProperty("sort", String.class, null);
				tableConclusions.setColumnHeader("sort", "Sort");

				tableConclusions.addContainerProperty("description", String.class, null);
				tableConclusions.setColumnHeader("description", "Description");

				for (Conclusion c : meeting.getConclusions()) {
					Object newItemId = tableConclusions.addItem();
					Item row = tableConclusions.getItem(newItemId);
					row.getItemProperty("sort").setValue(getSortName(c.getSort()));
					row.getItemProperty("description").setValue(getConclusionDescription(c));
				}
				rightSubLayout.addComponent(tableConclusions);

				mainLayout.addComponent(leftSubLayout);
				mainLayout.addComponent(centerSubLayout);
				mainLayout.addComponent(rightSubLayout);
				meetingDetailsPanel.setContent(mainLayout);

				meetingDetailsPanel.setVisible(true);
				meetingDetailsPanel.setHeight(meetingDetailsPanelHeight);

				tableMeetings.setHeight(tableMeetingsHeightReduced);

			}
		});

		return showDetailsButton;

	}

	private String getSortName(byte sortId) {

		if (sortId == 1) {
			return "Contract";
		} else if (sortId == 2) {
			return "Invoice";
		} else if (sortId == 3) {
			return "Meeting";
		} else {
			return "";
		}

	}

	private String getConclusionDescription(Conclusion c) {

		if (c == null) {
			return "";
		}

		if (c.getSort() == 1 && c.getContract() != null) {
			return c.getContract().getTitle();
		} else if (c.getSort() == 2 && c.getInvoice() != null) {
			return c.getInvoice().getService();
		} else if (c.getSort() == 3 && c.getMeeting() != null) {
			return c.getMeeting().getTitle();
		} else {
			return "Error";
		}

	}

}
