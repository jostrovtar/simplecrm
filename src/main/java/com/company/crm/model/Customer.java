package com.company.crm.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the customer database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "Customer.findAll", query = "SELECT c FROM Customer c"),
		@NamedQuery(name = "Customer.findFiltered", 
					query = "SELECT c FROM Customer c WHERE c.firstName like :filter or c.lastName like :filter or :filter = null")
})

public class Customer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.DATE)
	private Date birthdate;

	private String email;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	private String phone;

	// bi-directional many-to-many association to Meeting
	@ManyToMany
	@JoinTable(name = "customer_meeting", joinColumns = { @JoinColumn(name = "id_customer") }, inverseJoinColumns = {
			@JoinColumn(name = "id_meeting") })
	private List<Meeting> meetings;

	// //bi-directional many-to-many association to Conclusion
	// @ManyToMany
	// @JoinTable(
	// name="meeting_conclusion"
	// , joinColumns={
	// @JoinColumn(name="id_customer")
	// }
	// , inverseJoinColumns={
	// @JoinColumn(name="id_conclusion")
	// }
	// )
	// private List<Conclusion> conclusions;

	public Customer() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getBirthdate() {
		return this.birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public List<Meeting> getMeetings() {
		return this.meetings;
	}

	public void setMeetings(List<Meeting> meetings) {
		this.meetings = meetings;
	}

	// public List<Conclusion> getConclusions() {
	// return this.conclusions;
	// }
	//
	// public void setConclusions(List<Conclusion> conclusions) {
	// this.conclusions = conclusions;
	// }

}