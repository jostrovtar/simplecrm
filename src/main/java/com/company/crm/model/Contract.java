package com.company.crm.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the contract database table.
 * 
 */
@Entity
@NamedQuery(name="Contract.findAll", query="SELECT c FROM Contract c")
public class Contract implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Lob
	private String content;

	@Temporal(TemporalType.DATE)
	private Date date;

	private String title;

	//bi-directional many-to-one association to Conclusion
	@OneToMany(mappedBy="contract")
	private List<Conclusion> conclusions;

	public Contract() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Conclusion> getConclusions() {
		return this.conclusions;
	}

	public void setConclusions(List<Conclusion> conclusions) {
		this.conclusions = conclusions;
	}

	public Conclusion addConclusion(Conclusion conclusion) {
		getConclusions().add(conclusion);
		conclusion.setContract(this);

		return conclusion;
	}

	public Conclusion removeConclusion(Conclusion conclusion) {
		getConclusions().remove(conclusion);
		conclusion.setContract(null);

		return conclusion;
	}

}