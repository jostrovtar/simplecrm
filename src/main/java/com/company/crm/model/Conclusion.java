package com.company.crm.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the conclusion database table.
 * 
 */
@Entity
@NamedQuery(name="Conclusion.findAll", query="SELECT c FROM Conclusion c")
public class Conclusion implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private byte sort;

	//bi-directional many-to-one association to Contract
	@ManyToOne
	@JoinColumn(name="id_contract")
	private Contract contract;

	//bi-directional many-to-one association to Invoice
	@ManyToOne
	@JoinColumn(name="id_invoice")
	private Invoice invoice;

	//bi-directional many-to-one association to Meeting
	@ManyToOne
	@JoinColumn(name="id_meeting")
	private Meeting meeting;

//	//bi-directional many-to-many association to Meeting
//	@ManyToMany(mappedBy="conclusions")
//	private List<Meeting> meetings;

	public Conclusion() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte getSort() {
		return this.sort;
	}

	public void setSort(byte sort) {
		this.sort = sort;
	}

	public Contract getContract() {
		return this.contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public Invoice getInvoice() {
		return this.invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public Meeting getMeeting() {
		return this.meeting;
	}

	public void setMeeting(Meeting meeting) {
		this.meeting = meeting;
	}

//	public List<Meeting> getMeetings() {
//		return this.meetings;
//	}
//
//	public void setMeetings(List<Meeting> meetings) {
//		this.meetings = meetings;
//	}

}