package com.company.crm.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the meeting database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Meeting.findAll", query="SELECT m FROM Meeting m"),
	@NamedQuery(name="Meeting.findByCustomer", query="SELECT m FROM Meeting m join m.customers c WHERE c.id = :cId")
})

public class Meeting implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date end;

	private String location;

	@Temporal(TemporalType.TIMESTAMP)
	private Date start;

	private String title;

	//bi-directional many-to-one association to Conclusion
//	@OneToMany(mappedBy="meeting")
//	private List<Conclusion> conclusions;
	
	@ManyToMany
	@JoinTable(
			name="meeting_conclusion"
			, joinColumns={
					@JoinColumn(name="id_meeting")
					}
				, inverseJoinColumns={
					@JoinColumn(name="id_conclusion")
					}
				)
	private List<Conclusion> conclusions;

	//bi-directional many-to-many association to Customer
	@ManyToMany(mappedBy="meetings")
	private List<Customer> customers;

	public Meeting() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getEnd() {
		return this.end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Date getStart() {
		return this.start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Conclusion> getConclusions() {
		return this.conclusions;
	}

	public void setConclusions(List<Conclusion> conclusions) {
		this.conclusions = conclusions;
	}

	public Conclusion addConclusion(Conclusion conclusion) {
		getConclusions().add(conclusion);
		conclusion.setMeeting(this);

		return conclusion;
	}

	public Conclusion removeConclusion(Conclusion conclusion) {
		getConclusions().remove(conclusion);
		conclusion.setMeeting(null);

		return conclusion;
	}

	public List<Customer> getCustomers() {
		return this.customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

}