package com.company.crm.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the invoice database table.
 * 
 */
@Entity
@NamedQuery(name="Invoice.findAll", query="SELECT i FROM Invoice i")
public class Invoice implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	private double price;

	private String service;

	//bi-directional many-to-one association to Conclusion
	@OneToMany(mappedBy="invoice")
	private List<Conclusion> conclusions;

	public Invoice() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getPrice() {
		return this.price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getService() {
		return this.service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public List<Conclusion> getConclusions() {
		return this.conclusions;
	}

	public void setConclusions(List<Conclusion> conclusions) {
		this.conclusions = conclusions;
	}

	public Conclusion addConclusion(Conclusion conclusion) {
		getConclusions().add(conclusion);
		conclusion.setInvoice(this);

		return conclusion;
	}

	public Conclusion removeConclusion(Conclusion conclusion) {
		getConclusions().remove(conclusion);
		conclusion.setInvoice(null);

		return conclusion;
	}

}